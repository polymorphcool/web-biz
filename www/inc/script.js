// GLOBAL
const languages = [ 'fr', 'nl', 'en' ]
const language_default = languages[0]
let language_current = null;
let content_all = null;
let content_current = null;

const proj = document.getElementById('proj');
const proj_tabs = document.getElementById('project_tabs');
const about = document.getElementById('about'); // << TO REMOVE!!!
const right = document.getElementById('right');
const projects = document.getElementById('projects');
const intro = document.getElementById('intro');
const intro_text = document.getElementById('intro_text');
const footer = document.getElementById("footer");
const document_title_default = document.title;
const document_title_icons = [ "⊙︿⊙", "(ノ´∀｀)ノ", "＼(^◇^)／", "＼(^◇^)／", "(･ω･ﾉﾉﾞ", "(^.^)/", "♡＾▽＾♡", "(＾▽＾)", "(⌒‿⌒)", "(⌒‿⌒)", "(･ω･)b", "ʘ‿ʘ", "മ◡മ" ];

// when opening project, this stores the project element
// see openProject & closeProject for usage
let project_selected = null;
// flags for url
let about_open = false;
let contact_open = false;

// will be turned into an array once content is loaded 
let content_intro = null
let target_color_degree = 90;
let current_color_degree = 90;
const default_color = [211,211,211,1];
let target_colors = [ [211,211,211,1], [211,211,211,1], [211,211,211,1], [211,211,211,1] ];
let current_colors = [ [211,211,211,1], [211,211,211,1], [211,211,211,1], [211,211,211,1] ];

/*==== UTILS =====*/

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

function getRandomFloat(min, max) {
	return Math.random() * (max - min) + min;
}

function getRandomColor( limits_red, limits_green, limits_blue ) {
	let c = 'rgb(';
	for ( let i=0; i < 3; ++i ) {
		let n = 0;
		if ( i == 0 ) { n = getRandomInt( limits_red[0], limits_red[1] ); }
		else if ( i == 1 ) { n = getRandomInt( limits_green[0], limits_green[1] ); }
		else { n = getRandomInt( limits_blue[0], limits_blue[1] ); }
		if ( i > 0 ) {
			c += ",";
		}
		c += n;
	}
	return c + ')';
}

/*==== MENU =====*/

function hide_all() {
	current_color_degree = 0;
	var color_count = target_colors.length; 
	for ( let i=0; i < color_count; i++ ) {
		for ( let j=0; j < 3; ++j ) {
			target_colors[i][j] = default_color[j];
		}
	}
	let spans = intro_text.getElementsByTagName( 'span' );
	for ( let i=0; i < spans.length; ++i ) {
		spans[i].style.color = 'rgb('+255+','+255+','+255+')';
	}
	hideAbout();
	clearProjects();
}

// regenerate content of projects div
function showProjects() {
	
	if (content_current == null) {
		return;
	}
	if ( projects.innerHTML != '' ) {
		clearProjects();
		return;
	}
	
	intro.classList.add("intro_blur");
	
	hideAbout();
	hideContact();
	
	// regeneration of projects HTML
	projects.innerHTML = '';
	
	for (let i=0; i < content_current.projects.length; ++i) {
		
		let project = content_current.projects[i];
		
		var wrpr = document.createElement("div");
		wrpr.className = "project";
		wrpr.id = project.name;

		//let randSeconds = getRandomInt(1,7);
		//wrpr.style.animation = "movement " + randSeconds +"s linear infinite";
		wrpr.onclick = function() { event.stopPropagation(); openProject(this); };
		
		// content will be loaded by calling translate_project at the end of this block
		var pdesc = document.createElement("div");
		pdesc.className = "project_desc";
		wrpr.appendChild( pdesc );
		
		var close = document.createElement("div");
		close.className = "close";
		close.innerHTML = "×";
		close.onclick = function() { event.stopPropagation(); closeProject(); };
		wrpr.appendChild( close );
		
		var gallery = document.createElement("div");
		gallery.className = "project_img";
		for (let j=0; j < project.media.length; ++j ) {
			let media = project.media[j];
			if ( media.type == 'image' ) {
				gallery.appendChild( image_thumb( media ) );
			} else if ( media.type == 'video' ) {
				gallery.appendChild( video_thumb( media ) );
			}
		}
		wrpr.appendChild( gallery );
		projects.appendChild( wrpr );
		
		translate_project( project );
		
	}
	
}

function showAbout() {
	
	clearProjects();
	hideContact();
	right.classList.toggle("rightHaut");
	if ( right.classList.contains("rightHaut") ) {
		right.scrollTo(0, 0);
	}
	about_open = right.classList.contains("rightHaut");
	update_url();
	
}

function hideAbout() {
	
	if(right.classList.contains("rightHaut")) {
		right.classList.remove("rightHaut");
	}
	about_open = false;
	update_url();
	
}

function showContact() {
	
	footer.classList.toggle("contact_visible");
	
	contact_open = footer.classList.contains("contact_visible");
	update_url();
	
}

function hideContact() {
	
	if(footer.classList.contains("contact_visible")) {
		footer.classList.remove("contact_visible");
	}
	
	contact_open = false;
	update_url();
	
}

/*==== PROJECTS MANAGEMENT =====*/

// clear all projects from document
function clearProjects() {
	
	project_selected = null;
	projects.innerHTML = '';
	intro.style.filter = '';
	intro.classList.remove("intro_blur");
	
}

// generate a valid image thumb tag for gallery
function image_thumb( media ) {
	
	var img = document.createElement("img");
	img.className = "img_proj";
	img.src = media.thumb;
	img.alt = "image";															// 0
	img.alt += ":"+media.full;													// 1
	img.alt += ":"+media.thumb;													// 2
	return img;

}

// generate a valid video thumb tag for gallery
function video_thumb( media ) {
	
	var img = document.createElement("img");
	img.className = "img_proj";
	img.src = media.thumb;
	img.alt = "video";															// 0
	img.alt += ":"+media.full;													// 1
	img.alt += ":"+media.thumb;													// 2
	img.alt += ":"+media.full.substring(media.full.length-3,media.full.length);	// 3
	img.alt += ":"+media.full_size.width;										// 4
	img.alt += ":"+media.full_size.height;										// 5
	return img;

}

// loads HD images and video if open = true
// loads thumbs for all media if open = false
function enableGallery( project, open ) {
	
	let galleries = project.getElementsByClassName("project_img");
	if ( galleries.length == 0 ) {
		return;
	}
	let gallery = galleries[0];
	let media = Array.from(gallery.children);
	let alts = []
	for ( let i=0; i < media.length; ++i ) {
		let element = media[i];
		if ( element.tagName != "IMG" && element.tagName != "VIDEO" ) {
			continue;
		}
		alts.push( element.alt );
	}
	gallery.innerHTML = '';
	for ( let i=0; i < alts.length; ++i ) {
		let alt = alts[i];
		let data = alt.split(":");
		if ( data.length < 3 ) {
			continue;
		}
		if ( open ) {
			if ( data[0] == 'image' ) {
				
				// must be 3 arguments to be valid, see image_thumb
				if ( data.length != 3 ) {
					continue;
				}
				var img = document.createElement("img");
				img.className = "img_proj";
				img.alt = alt;
				img.src = data[1];
				gallery.appendChild( img );
				
			} else if ( data[0] == 'video' ) {
				
				// must be 6 arguments to be valid, see video_thumb
				if ( data.length != 6 ) {
					continue;
				}
				
				var video = document.createElement("video");
				video.alt = alt;
				video.controls = "controls";
				video.width = data[4];
				video.height = data[5];
				var src = document.createElement("source");
				if ( data[3] == 'mp4' ) {
					src.type = "video/mp4";
				}
				src.src = data[1];
				video.appendChild(src);
				gallery.appendChild( video );
				
			}
		} else {
			var img = document.createElement("img");
			img.className = "img_proj";
			img.alt = alt;
			img.src = data[2];
			gallery.appendChild( img );
		}
	}
	
}

// called when a minimized project is clicked
// close previous project if some by calling
// see showProjects() for generation
function openProject( element ) {
	
	if ( element == project_selected ) {
		return;
	}
	if ( element == null || project_selected != null ) {
		closeProject();
	}
	
	enableGallery( element, true );
	
	const children = element.children;
	element.classList.add("project_deployed");
	children[0].classList.add("project_desc_visible");
	children[1].classList.add("close_visible");
	children[2].classList.add("project_img_visible");
	const images = children[2].children;
	Array.from(images).forEach(img => {
		img.classList.add("img_deployed");
	});
	
	project_selected = element;
	update_url();

	element.scrollIntoView({ behavior: 'smooth', block: 'start'});
	
}

// called when the project's close button is clicked
// see showProjects() for generation
function closeProject() {
	
	if (project_selected == null) {
		return;
	}
	
	enableGallery( project_selected, false );
	
	window.scrollTo(0, 0);
	if(project_selected.classList.contains("project_deployed")) {
		project_selected.classList.remove("project_deployed");
		const children = project_selected.children;
		children[0].classList.remove("project_desc_visible");
		children[1].classList.remove("close_visible");
		children[2].classList.remove("project_img_visible");
		const images = children[2].children;
		Array.from(images).forEach(img => {
		  img.classList.remove("img_deployed");
		});
	}
	
	project_selected = null;
	update_url();
	
}

/*==== INTRO MANAGEMENT =====*/

function hover_intro( element ) {
	
	if (content_intro == null) {
		return;
	}
	
	let text = element.innerHTML.trim();
	// searching text in lists
	var current_line = -1;
	var current_word = -1;
	for ( let i=0; i < content_intro.length; ++i ) {
		for ( let j=0; j < content_intro[i].length; ++j ) {
			if (content_intro[i][j] == text) {
				current_line = i;
				current_word = j;
			}
		}
	}
	if ( current_line == -1 || current_word == -1 ) {
		return;
	}
	let newline = getRandomInt(0,2);
	if ( newline == current_line ) {
		newline = (newline+1) % 3;
	}
	element.innerHTML = content_intro[newline][current_word];
	
	let dominant = getRandomInt(0,4);
	let red = 0;
	let green = 0;
	let blue = 0;
	if ( dominant == 0 ) {
		red = getRandomInt( 120, 200 );
		green = getRandomInt( 50, 180 );
		blue = getRandomInt( 50, 180 );
	} else if ( dominant == 1 ) {
		red = getRandomInt( 50, 180 );
		green = getRandomInt( 120, 200 );
		blue = getRandomInt( 50, 180 );
	} else if ( dominant == 2 ) {
		red = getRandomInt( 50, 180 );
		green = getRandomInt( 50, 180 );
		blue = getRandomInt( 120, 200 );
	} else {
		red = getRandomInt( 120, 200 );
		green = getRandomInt( 120, 200 );
		blue = getRandomInt( 120, 200 );
	}
	
	// color backup
	for ( let i=1; i < target_colors.length; i++ ) {
		target_colors[i-1] = target_colors[i];
	}
	target_colors[target_colors.length-1] = [ red, green, blue, element.innerHTML.length * 4 ];
	target_color_degree += 1;
	
	if ( projects.innerHTML == '' && !about_open ) {
		element.style.color = 'rgb('+255+','+255+','+255+')';
	} else {
		let grey = getRandomInt( 190, 255 );
		element.style.color = 'rgb('+grey+','+grey+','+grey+')';
	}
	
}

let hover_animation = setInterval(function() {
	
	if ( projects.innerHTML == '' && !about_open ) {
		
		// reaching targets
		if ( current_color_degree != target_color_degree ) {
			
			current_color_degree += (target_color_degree-current_color_degree) * 0.1;
			if ( Math.abs(current_color_degree-target_color_degree) < 0.001 ) {
				current_color_degree = target_color_degree;
			}
			var color_count = target_colors.length; 
			var percent = 0;
			var gradient = "linear-gradient("+current_color_degree+"deg,";
			var total_weight = 0
			for ( let i=0; i < color_count; i++ ) {
				for ( let j=0; j < 4; ++j ) {
					current_colors[i][j] += (target_colors[i][j]-current_colors[i][j]) * 0.1;
				}
				total_weight += current_colors[i][3];
			}
			for ( let i=0; i < color_count; i++ ) {
				gradient += "rgb("+current_colors[i][0]+","+current_colors[i][1]+","+current_colors[i][2]+") "+percent+"%";
				if ( i < color_count-1 ) {
					gradient += ",";
				}
				percent += (current_colors[i][3]/total_weight) * 100.0;
			}
			gradient += ")";
			document.body.style.backgroundImage = gradient;
		}
	}
	
	}, 20
);

/*==== LANGUAGES MANAGEMENT =====*/

function translate_intro() {
	
	if ( intro_text == null ) {
		return;
	}
	
	// select intro line
	var ci = null;
	if ( language_current == 'fr' ) {
		ci = content_intro[0];
	} else if ( language_current == 'nl' ) {
		ci = content_intro[1];
	} else if ( language_current == 'en' ) {
		ci = content_intro[2];
	} else {
		return;
	}
	
	let spans = intro_text.getElementsByTagName( 'span' );
	for ( let i=0; i < spans.length; ++i ) {
		if ( i > ci.length ) {
			break;
		}
		spans[i].innerHTML = ci[i];
		spans[i].style.color = "white";
	}
	
}

function translate_project( project ) {
	
	var div = document.getElementById( project.name );
	
	if ( div == null ) {
		return;
	}
	var pdescs = div.getElementsByClassName( "project_desc" );
	if ( pdescs.length < 0 ) {
		return;
	}
	
	var pdesc = pdescs[0];
	pdesc.innerHTML = '';
	
	for (let j=0; j < project.content.length; ++j ) {
		
		let part = project.content[j];
		
		if ( part.type == 'title' ) {
			
			var title = document.createElement("div");
			title.className = "project_title";
			for (let k=0; k < part.content.length; ++k ) {
				var span = document.createElement("span");
				span.innerHTML = part.content[k];
				title.appendChild( span );
			}
			pdesc.appendChild( title );
			
		} else if ( part.type == 'desc' ) {
			
			var desc = document.createElement("div");
			desc.className = "project_text";
			for (let k=0; k < part.content.length; ++k ) {
				var p = document.createElement("p");
				p.innerHTML = part.content[k];
				desc.appendChild( p );
			}
			pdesc.appendChild( desc );
			
		}
	}
	
}

function translate_projects() {
	
	if ( projects.innerHTML == '' ) {
		return;
	}
	
	for (let i=0; i < content_current.projects.length; ++i) {
		translate_project( content_current.projects[i] );
	}
	
}

function translate_ui() {
	
	document.getElementById( 'block_project' ).innerHTML = content_current.interface.projects;
	document.getElementById( 'block_about' ).innerHTML = content_current.interface.about;
	document.getElementById( 'block_contact' ).innerHTML = content_current.interface.contact;
	document.getElementById( 'block_contact' ).innerHTML = content_current.interface.contact;
	document.getElementById( 'address' ).innerHTML = content_current.interface.address;
	document.getElementById( 'policy' ).innerHTML = content_current.interface.policy;
	
}

// generation of about section
function load_about_element( element ) {
	
	// unique ids for each content block
	let title_id = 0;
	let subtitle_id = 0;
	let block_id = 0;
	
	for (let i=0; i < element.content.length; ++i ) {
		
		let part = element.content[i];
		var wrpr = document.createElement("div");
		
		// creation of a title block
		if ( part.type == 'title' ) {
			
			// one div and one h1
			wrpr.className = 'titles titles_' + title_id;
			title_id += 1;
			
			var h1 = document.createElement("h1");
			for ( let j=0; j < part.content.length; ++j ) {
				var span = document.createElement("span");
				span.innerHTML = part.content[j];
				h1.appendChild( span );
			}
			
			wrpr.appendChild( h1 );
		
		// creation of a text block
		} else if ( part.type == 'block' ) {
			
			// nesting several divs
			wrpr.className = element.name + ' ' + element.name + block_id;
			
			var wrpr2 = document.createElement("div");
			wrpr2.className = 'bloc_texte';
			
			var wrpr3 = document.createElement("div");
			wrpr3.className = language_current;
			
			block_id += 1;
			
			for ( let j=0; j < part.content.length; ++j ) {
				var content = part.content[j];
				if ( content.search( "<h" ) == 0 ) {
					wrpr3.innerHTML += content;
				} else {
					var p = document.createElement("p");
					p.innerHTML = part.content[j];
					wrpr3.appendChild( p );
				}
			}
			
			wrpr2.appendChild( wrpr3 );
			wrpr.appendChild( wrpr2 );
			
		// do nothing
		} else {
			
			continue;
			
		}
		
		// push wrapper into about
		right.appendChild( wrpr );
	
	}
	
}

function translate_about() {
	// cleaning about div
	right.innerHTML = '';
	for (let i=0; i < content_current.about.length; ++i) {
		let element = content_current.about[i];
		load_about_element( element )
	}
}

function load_lang( lang ) {
	
	// checking there is some content
	if ( content_all == null ) {
		console.log( 'no content loaded!!!' );
		return;
	}
	// checking requested language exists
	if ( languages.indexOf(lang) < 0) {
		console.log( 'invalid language! '+ lang );
		return;
	}
	
	// registration of current language
	language_current = lang;
	
	// loading language into current content
	content_current = content_all[lang];
	
	// loading projects and about
	translate_ui();
	translate_projects();
	translate_about();
	translate_intro();
	
	update_url();
	
}

function load_intro() {
	
	content_intro = [];
	for ( let i=0; i < languages.length; ++i ) {
		content_intro.push( content_all[languages[i]].interface.intro );
	}
	
}

/*==== URL =====*/

function update_url() {
	
	let urlargs = "#l:"+language_current;
	if (project_selected != null) {
		urlargs += ".p:"+project_selected.id;
	}
	if (about_open) {
		urlargs += ".a";
	}
	if (contact_open) {
		urlargs += ".c";
	}
	var href = window.location.href;
	if ( href.search("#") != -1 ) {
		href = href.substring( 0, href.search("#") );
	}
	window.location.href = href + urlargs;
	
	document.title = document_title_default;
	if (project_selected != null) {
		document.title = document_title_default+':'+project_selected.id + ' ' + document_title_icons[ getRandomInt(0,document_title_icons.length) ];
	}
	
}

function parse_url() {
	
	let requests = { 'project': null, 'about': false, 'contact': false };
	let parts = window.location.href.split('#');
	if (parts.length < 2) {
		return;
	}
	let args = parts[1].split('.');
	if (args.length == 0) {
		return;
	}
	for ( let i=0; i < args.length; ++i ) {
		let arg = args[i];
		let marker = arg.substring(0,1);
		// language request
		if ( marker == 'l' ) {
			var ls = arg.split(':');
			if ( ls.length != 2 ) {
				continue;
			}
			for ( let l=0; l < languages.length; ++l ) {
				if ( ls[1] == languages[l] ) {
					language_current = languages[l];
					break;
				}
			}
		// project request
		} else if ( marker == 'p' ) {
			var ps = arg.split(':');
			if ( ps.length != 2 ) {
				continue;
			}
			requests.project = ps[1];
		// about request
		} else if ( marker == 'a' ) {
			requests.about = true;
		// contact request
		} else if ( marker == 'c' ) {
			requests.contact = true;
		}
	}
	
	return requests;
	
}

/*==== CONTENT MANAGEMENT =====*/

// loading a json
function loadJSON(callback) {   
	var xobj = new XMLHttpRequest();
	xobj.overrideMimeType("application/json");
	xobj.open('GET', 'content.json', true);
	xobj.onreadystatechange = function () {
		if (xobj.readyState == 4 && xobj.status == "200") {
			callback(xobj.responseText);
		}
	};
	xobj.send(null);  
}

// let's start content loading
loadJSON(function(response) {
	
	content_all = JSON.parse( response );
	language_current = language_default;
	
	let urlreq = parse_url();
	
	load_intro();
	load_lang( language_current );
	
	if ( urlreq.project != null ) {
		showProjects();
		openProject( document.getElementById( urlreq.project ) );
	} else if ( urlreq.about ) {
		showAbout();
	}
	
	if ( urlreq.contact ) {
		showContact();
	}
	
	update_url();
	
});
