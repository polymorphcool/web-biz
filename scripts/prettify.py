#!/usr/bin/python3

# sudo apt install python3-pip
# pip3 install BeautifulSoup

import re, bs4

orig_prettify = bs4.BeautifulSoup.prettify
r = re.compile(r'^(\s*)', re.MULTILINE)
def prettify(self, encoding=None, formatter="minimal", indent_width=4):
    return r.sub(r'\1' * indent_width, orig_prettify(self, encoding, formatter))
bs4.BeautifulSoup.prettify = prettify

txt = ''
with open('../www/index.html','r') as f:
    txt = f.read()
soup = bs4.BeautifulSoup(txt)
prettyHTML = soup.prettify(indent_width=4)

f = open('../www/index.html','w')
f.write(prettyHTML)
f.close()
