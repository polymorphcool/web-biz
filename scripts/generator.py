#!/usr/bin/python3

# sudo apt install ffmpeg
# pip3 install ffmpeg-python

import os,time,enum,re,shutil,sys,json,re,bs4,ffmpeg # cv2
from distutils.dir_util import copy_tree
import PIL
from PIL import Image,ImageEnhance

LANGUAGES = 			['fr','nl','en']

CONTENT_FOLDER = 		"../content/"
CONTENT_PROJECTS = 		os.path.join( CONTENT_FOLDER, 'projects' )
CONTENT_ABOUT = 		os.path.join( CONTENT_FOLDER, 'about' )

CONTENT_IMAGES = 		['.gif','.jpg','.png','.jpeg']
CONTENT_VIDEOS = 		['.mp4','.mkv']

TEMP_FOLDER = 			'../tmp'

WEB_FOLDER = 			"../www/"
WEB_THUMB = 			[None,200]
WEB_MEDIA = 			os.path.join( WEB_FOLDER, 'media' )
WEB_JSON = 				os.path.join( WEB_FOLDER, 'content.json' )

interface = {}
projects = []
about = []

def image_size( path ):
	try:
		img = Image.open( path )
		return img.size
	except:
		return [0,0]

def video_size( path ):
	try:
		probe = ffmpeg.probe( path )
		video_streams = [stream for stream in probe["streams"] if stream["codec_type"] == "video"]
		return video_streams[0]
	except Exception as e:
		print( e )
		return [0,0]

def image_thumb( filename, path ):
	
	try:
		
		if not os.path.isdir( TEMP_FOLDER ):
			os.makedirs( TEMP_FOLDER )
		
		trgt = os.path.join(TEMP_FOLDER,filename)
		img = Image.open( path )
		
		thumb = [WEB_THUMB[0],WEB_THUMB[1]]
		box = [0,0,img.size[0],img.size[1]]
		if thumb[0] == None:
			thumb[0] = int(thumb[1] / img.size[1] * img.size[0])
		elif thumb[1] == None:
			thumb[1] = int(thumb[0] / img.size[0] * img.size[1])
		
		img = img.resize( thumb, resample=PIL.Image.BICUBIC, box=box )
		img.save( trgt )
		
		return trgt
		
	except Exception as e:
		
		print( 'failed! ', path )
		print( e )
		return None

def video_thumb( filename, path ):
	
	try:
		
		if not os.path.isdir( TEMP_FOLDER ):
			os.makedirs( TEMP_FOLDER )
		
		tmp_image = os.path.join( TEMP_FOLDER,'.tmp_vid_thumb.jpg' )
		os.system('ffmpeg -y -i '+path+' -ss 00:00:10.000 -vframes 1 ' + tmp_image)
		
		return image_thumb( filename, tmp_image )
		
	except Exception as e:
		
		print( 'failed! ', path )
		print( e )
		return None

def clean_string( path ):
	return path.replace(' ','_').replace('(','').replace(')','')

def content_folder( target, name, path ):
	
	files = os.listdir( path )
	files.sort()
	
	data = {
		'name': clean_string( name[4:] ),
		'content_path': path,
		'media': []
	}
	
	err = None
	for lang in LANGUAGES:
		cf = 'content_'+lang+'.json'
		if cf in files:
			p = os.path.join(path,cf)
			with open(p,'r') as f:
				data[lang] = f.read()
		else:
			if err != None:
				err += '\n'
			else:
				err = ''
			err += 'content_'+lang+'.json missing in folder '+path
	
	if err != None:
		print(err)
		return
	
	for f in files:
		
		for ci in CONTENT_IMAGES:
		
			if f[-len(ci):] == ci:
				fname = clean_string( name[4:]+'.'+f )
				d = { 
				'type': 'image',
				'filename': fname,
				'thumbname': fname,
				'full': os.path.join( path, f ), 
				'thumb': image_thumb( fname, os.path.join(path,f) )
				}
				d['full_size'] = image_size( d['full'] )
				d['thumb_size'] = image_size( d['thumb'] )
				data['media'].append( d )
		
		for cv in CONTENT_VIDEOS:
		
			if f[-len(cv):] == cv:
				fname = clean_string( name[4:]+'.'+f )
				d = { 
				'type': 'video',
				'filename': fname,
				'thumbname': fname.replace( cv, '.jpg' ),
				'full': os.path.join( path, f ), 
				'thumb': video_thumb( fname.replace( cv, '.jpg' ), os.path.join(path,f) )
				}
				d['full_size'] = video_size( d['full'] )
				d['thumb_size'] = image_size( d['thumb'] )
				data['media'].append( d )
	
	target.append( data )

def content_load():
	
	for l in LANGUAGES:
		ip = os.path.join( CONTENT_FOLDER, 'interface_'+l+'.json' )
		with open( ip,'r' ) as f:
			interface[l] = f.read()
	
	for dirpath, dirs, files in os.walk(CONTENT_PROJECTS):
		dirs.sort()
		for d in dirs:
			content_folder( projects, d, os.path.join(dirpath,d) )
	
	for dirpath, dirs, files in os.walk(CONTENT_ABOUT):
		dirs.sort()
		for d in dirs:
			content_folder( about, d, os.path.join(dirpath,d) )

def move_media():
	
	for i in range(0,2):
		
		elements = None
		
		if i == 0:
			elements = projects
		elif i == 1:
			elements = about
		
		if elements != None:
			
			for element in elements:
				
				pfolder_full = os.path.join( WEB_MEDIA, element['name'], 'full' )
				if os.path.isdir( pfolder_full ):
					shutil.rmtree( pfolder_full )
				os.makedirs( pfolder_full )
				
				pfolder_thumb = os.path.join( WEB_MEDIA, element['name'], 'thumb' )
				if os.path.isdir( pfolder_thumb ):
					shutil.rmtree( pfolder_thumb )
				os.makedirs( pfolder_thumb )
				
				for media in element['media']:
					
					dst = os.path.join( pfolder_full, media['filename'][len(element['name'])+1:] )
					shutil.copyfile( media['full'], dst )
					media['full'] = dst.replace( WEB_FOLDER, '' )
					
					dst = os.path.join( pfolder_thumb, media['thumbname'][len(element['name'])+1:] )
					shutil.copyfile( media['thumb'], dst )
					media['thumb'] = dst.replace( WEB_FOLDER, '' )

def replace_images( txt, media ):
	
	while txt.find( '%IMG:' ) != -1:
		start = txt.find( '%IMG:' )
		end = txt.find( '%', start+1 )
		needle = txt[start+5:end]
		found = False
		for m in media:
			if m['full'].find( needle ) != -1:
				txt = txt[:start]+m['full']+txt[end+1:]
				found = True
				break
		if not found:
			txt = txt[:start]+txt[end+1:]
	
	return txt

def generate_json():
	
	out = {}
	
	for lang in LANGUAGES:
		
		out[lang] = {}
		out[lang]['projects'] = []
		out[lang]['about'] = []
		
		if lang in interface:
			out[lang]['interface'] = json.loads( interface[lang] )
		else:
			print( "missing "+lang+" translation for interface!" )
			return False
		
		for i in range(0,2):
			
			elements = []
			key = None
			
			if i == 0:
				key = 'projects'
				elements = projects
			elif i == 1:
				key = 'about'
				elements = about
		
			if key != None:
				for element in elements:
					
					# replacing images path in content
					content = replace_images( element[lang], element['media'] )
					
					p = {
						'name': element['name'],
						'content': json.loads( content ),
						'media': []
					}
					
					for media in element['media']:
						p['media'].append( {
							'type': media['type'],
							'full': media['full'],
							'thumb': media['thumb'],
							'full_size': media['full_size'],
							'thumb_size': media['thumb_size']
						} )
					out[lang][key].append( p )
	
	with open( WEB_JSON, 'w' ) as f:
		f.write( json.dumps(out, indent=4) )
		return True
	
	return False

def clear_tmp():
	if os.path.isdir( TEMP_FOLDER ):
		shutil.rmtree( TEMP_FOLDER )

content_load()

move_media()

if generate_json():
	print( "JSON successfully generated!" )

clear_tmp()
