#!/usr/bin/python3
import os,time,enum,re,shutil,sys,json, re, bs4
from distutils.dir_util import copy_tree
from PIL import Image,ImageEnhance

TMPL_PATH = 					'../template/skin/index.html'
PROJECTS_PATH = 				'../assets/'
PROJECT_FILE = 					'project.json'
PROJECT_ID_STRIP = 				5
SECTION_FILE = 					'section.json'
SECTION_ID_STRIP = 				5

IMAGE_FOLDER = 					'images'
VIDEO_FOLDER = 					'images'
OUTPUT_IMAGE_FOLDER =			'../www/images/'
OUTPUT_VIDEO_FOLDER =			'../www/images/'
OUTPUT_FILE =					'../www/tmp.html'

TMPL_TAG_TMP = 					"TMP"
TMPL_TAG_SECTION = 				"SECTION"
TMPL_TAG_SECTION_TITLE = 		"SECTION_TITLE"
TMPL_TAG_SECTION_BLOCK = 		"SECTION_BLOCK"
TMPL_TAG_SECTION_BLOCKLINE = 	"SECTION_BLOCKLINE"
TMPL_TAG_SECTION_MENU = 		"SECTION_MENU"
TMPL_TAG_SECTION_MENULINK = 	"SECTION_MENULINK"
TMPL_TAG_PROJECT_TAB = 			"PROJECT_TAB"
TMPL_TAG_PROJECT_TITLE = 		"PROJECT_TITLE"
TMPL_TAG_PROJECT_DESC = 		"PROJECT_DESC"
TMPL_TAG_PROJECT_DESCLINE = 	"PROJECT_DESCLINE"
TMPL_TAG_SLIDESHOW_VIDEO = 		"SLIDESHOW_VIDEO"
TMPL_TAG_SLIDESHOW_IMAGE = 		"SLIDESHOW_IMAGE"

TMPL_MARKER_ID = 				"ID"
TMPL_MARKER_SRC = 				"SRC"
TMPL_MARKER_SECTION_LIST = 		"SECTION_LIST"
TMPL_MARKER_SECTION_MENU_LIST = "SECTION_MENU_LIST"
TMPL_MARKER_TITLES = 			"TITLES"
TMPL_MARKER_BLOCKS = 			"BLOCKS"
TMPL_MARKER_DESCS = 			"DESCS"
TMPL_MARKER_SLIDESHOW = 		"SLIDESHOW"
TMPL_MARKER_PROJECT_LIST = 		"PROJECT_LIST"
TMPL_MARKER_TITLE = 			"TITLE"
TMPL_MARKER_ALT = 				"ALT"
TMPL_MARKER_LANG = 				"LANG"
TMPL_MARKER_CLASS = 			"CLASS"
TMPL_MARKER_CONTENT = 			"CONTENT"
TMPL_MARKER_TAB_CLASSES = 		"TAB_CLASSES"
TMPL_MARKER_OVERLAY_TEXT = 		"OVERLAY_TEXT"

class html_parser:
	
	def __init__(self):
		self.tmp = None
		self.outer = None
		self.inner = None
	
	def replace_marker( self, content, marker, text ):
		return content.replace( '#'+marker+'#', text )
	
	def find_marker( self, content, marker ):
		return content.find( '#'+marker+'#' )
	
	def clear_tmp( self, content ):
		self.tmp = None
		self.extract_tag( content, TMPL_TAG_TMP )
		while self.outer != None:
			content = content.replace( self.outer, '' )
			self.extract_tag( content, TMPL_TAG_TMP )
		return content
	
	def extract_tag( self, content, tag ):
		self.tmp = None
		starttag = '<!--#'+tag+'_#-->'
		endtag = '<!--#_'+tag+'#-->'
		startat = content.find( starttag )
		endat = content.find( endtag )
		if startat < 0 or endat < 0 or endat < startat:
			self.outer = None
			self.inner = None
			return False
		self.outer = content[startat:endat+len(tag)+10]
		self.inner = content[startat+len(tag)+10:endat]
		return True

class project_tmpl:
	
	def __init__(self, content):
		self.content = content
		self.valid = False
		# subparts
		self.project_title = None
		self.project_desc = None
		self.project_descline = None
		self.slideshow_video = None
		self.slideshow_image = None
		self.parser = html_parser()
		self.parse()
	
	def parse(self):
		self.content = self.parser.clear_tmp(self.content)
		
		if self.parser.extract_tag( self.content, TMPL_TAG_PROJECT_TITLE ):
			self.project_title = self.parser.inner
			self.content = self.content.replace( self.parser.outer, '' )
			
		if self.parser.extract_tag( self.content, TMPL_TAG_PROJECT_DESC ):
			self.project_desc = self.parser.inner
			self.content = self.content.replace( self.parser.outer, '' )
		
		if self.project_desc != None and self.parser.extract_tag( self.project_desc, TMPL_TAG_PROJECT_DESCLINE ):
			self.project_descline = self.parser.inner
			self.project_desc = self.project_desc.replace( self.parser.outer, '' )
			
		if self.parser.extract_tag( self.content, TMPL_TAG_SLIDESHOW_VIDEO ):
			self.slideshow_video = self.parser.inner
			self.content = self.content.replace( self.parser.outer, '' )
		
		if self.parser.extract_tag( self.content, TMPL_TAG_SLIDESHOW_IMAGE ):
			self.slideshow_image = self.parser.inner
			self.content = self.content.replace( self.parser.outer, '' )
	
	def dump(self):
		print("\n###### class.project_tmpl dump")
		print(self)
		print( '\t content: ', self.content.replace( '  ', '' ).replace( '\t', '' ).replace( '\n', '' ) )
		print( '\t project_title: ', self.project_title.replace( '  ', '' ).replace( '\t', '' ).replace( '\n', '' ) )
		print( '\t project_desc: ', self.project_desc.replace( '  ', '' ).replace( '\t', '' ).replace( '\n', '' ) )
		print( '\t project_descline: ', self.project_descline.replace( '  ', '' ).replace( '\t', '' ).replace( '\n', '' ) )
		print( '\t slideshow_video: ', self.slideshow_video.replace( '  ', '' ).replace( '\t', '' ).replace( '\n', '' ) )
		print( '\t slideshow_image: ', self.slideshow_image.replace( '  ', '' ).replace( '\t', '' ).replace( '\n', '' ) )

class section_tmpl:
	
	def __init__(self, content):
		self.content = content
		self.valid = False
		# subparts
		self.section_title = None
		self.section_block = None
		self.section_blockline = None
		self.parser = html_parser()
		self.parse()
	
	def parse(self):
		self.content = self.parser.clear_tmp(self.content)
		if self.parser.extract_tag( self.content, TMPL_TAG_SECTION_TITLE ):
			self.section_title = self.parser.inner
			self.content = self.content.replace( self.parser.outer, '' )
		if self.parser.extract_tag( self.content, TMPL_TAG_SECTION_BLOCK ):
			self.section_block = self.parser.inner
			self.content = self.content.replace( self.parser.outer, '' )
		if self.section_block != None and self.parser.extract_tag( self.section_block, TMPL_TAG_SECTION_BLOCKLINE ):
			self.section_blockline = self.parser.inner
			self.section_block = self.section_block.replace( self.parser.outer, '' )
	
	def dump(self):
		print("\n###### class.section_tmpl dump")
		print(self)
		print( '\t content: ', self.content.replace( '  ', '' ).replace( '\t', '' ).replace( '\n', '' ) )
		print( '\t section_title: ', self.section_title.replace( '  ', '' ).replace( '\t', '' ).replace( '\n', '' ) )
		print( '\t section_block: ', self.section_block.replace( '  ', '' ).replace( '\t', '' ).replace( '\n', '' ) )
		print( '\t section_blockline: ', self.section_blockline.replace( '  ', '' ).replace( '\t', '' ).replace( '\n', '' ) )

class section_menu_tmpl:
	
	def __init__(self, content):
		self.content = content
		self.valid = False
		# subparts
		self.section_link = None
		self.parser = html_parser()
		self.parse()
	
	def parse(self):
		self.content = self.parser.clear_tmp(self.content)
		
		if self.parser.extract_tag( self.content, TMPL_TAG_SECTION_MENULINK ):
			self.section_link = self.parser.inner
			self.content = self.content.replace( self.parser.outer, '' )
	
	def dump(self):
		print("\n###### class.section_tmpl dump")
		print(self)
		print( '\t content: ', self.content.replace( '  ', '' ).replace( '\t', '' ).replace( '\n', '' ) )
		print( '\t section_link: ', self.section_link.replace( '  ', '' ).replace( '\t', '' ).replace( '\n', '' ) )

class tmpl:
	
	def __init__(self, path):
		self.content = None
		self.path = path
		self.project_tmpl = None
		self.section_tmpl = None
		self.section_menu_tmpl = None
		self.parser = html_parser()
		self.parse()
	
	def parse(self):
		f = open( self.path, 'r' )
		self.content = f.read()
		f.close()
		
		if self.parser.extract_tag( self.content, TMPL_TAG_PROJECT_TAB ):
			self.project_tmpl = project_tmpl( self.parser.inner )
			self.content = self.content.replace( self.parser.outer, '' )
		else:
			print(TMPL_TAG_PROJECT_TAB, ' not found')
			
		if self.parser.extract_tag( self.content, TMPL_TAG_SECTION ):
			self.section_tmpl = section_tmpl( self.parser.inner )
			self.content = self.content.replace( self.parser.outer, '' )
		else:
			print(TMPL_TAG_SECTION, ' not found')
			
		if self.parser.extract_tag( self.content, TMPL_TAG_SECTION_MENU ):
			self.section_menu_tmpl = section_menu_tmpl( self.parser.inner )
			self.content = self.content.replace( self.parser.outer, '' )
		else:
			print(TMPL_TAG_SECTION_MENU, ' not found')
		
		self.content = self.parser.clear_tmp( self.content )
	
	def dump(self):
		print("\n###### class.tmpl dump")
		print(self)
		print( '\t content: ', self.content.replace( '  ', '' ).replace( '\t', '' ).replace( '\n', '' ) )
		self.project_tmpl.dump()
		self.section_tmpl.dump()
		self.section_menu_tmpl.dump()

class content_loader:
	
	def __init__(self):
		self.projects = []
		self.sections = []
		files = os.listdir( PROJECTS_PATH )
		files.sort()
		for f in files:
			dp = os.path.join( PROJECTS_PATH, f )
			jp = os.path.join( dp, PROJECT_FILE )
			sp = os.path.join( dp, SECTION_FILE )
			if os.path.isdir(dp) and os.path.isfile(jp):
				self.projects.append( {
				'id': f[PROJECT_ID_STRIP:],
				'path': dp, 
				'data': json.load(open(jp,)) 
				} )
			elif os.path.isdir(dp) and os.path.isfile(sp):
				self.sections.append( {
				'id': f[SECTION_ID_STRIP:],
				'path': dp, 
				'data': json.load(open(sp,)) 
				} )
	
	def dump(self):
		print("\n###### class.content_loader dump")
		print(self)
		print( '\t projects: ', self.projects )	

class page_builder:
	
	def __init__(self):
		self.template = None
		self.content = None
		self.parser = html_parser()
	
	def set_template(self, obj):
		self.template = obj
	
	def set_content(self, obj):
		self.content = obj
	
	def clean_media(self, path):
		if os.path.isdir(path):
			shutil.rmtree(path)
	
	def publish_media(self, media):
		if not os.path.isfile( media['src'] ):
			return
		if not os.path.isdir( media['dst_dir'] ):
			os.makedirs( media['dst_dir'] )
		shutil.copyfile( media['src'], media['dst'] )
	
	def build(self):
		
		if self.template == None or self.content == None:
			return
		
		phtml = ''
		pcount = 0
		for p in self.content.projects:
			
			project_html = '' + self.template.project_tmpl.content
			project_html = self.parser.replace_marker( project_html, TMPL_MARKER_ID, p['id'] )
			if pcount == 0:
				project_html = self.parser.replace_marker( project_html, TMPL_MARKER_TAB_CLASSES, 'first opened' )
			else:
				project_html = self.parser.replace_marker( project_html, TMPL_MARKER_TAB_CLASSES, '' )
			
			overlay_text = ''
			for ot in p['data']['overlay_text']:
				if overlay_text != '':
					overlay_text += ' '
				overlay_text += '<span>'+ot+'</span>'
			project_html = self.parser.replace_marker( project_html, TMPL_MARKER_OVERLAY_TEXT, overlay_text )
			
			titles = ''
			descs = ''
			for lang in p['data']['lang']:
				
				title_html = '' + self.template.project_tmpl.project_title
				desc_html = '' + self.template.project_tmpl.project_desc
				line_html = '' + self.template.project_tmpl.project_descline
				
				title_html = self.parser.replace_marker( title_html, TMPL_MARKER_LANG, lang['name'] )
				desc_html = self.parser.replace_marker( desc_html, TMPL_MARKER_LANG, lang['name'] )
				line_html = self.parser.replace_marker( line_html, TMPL_MARKER_LANG, lang['name'] )
				
				for block in lang['content']:
					if block['type'] == 'title':
						text = ''
						for c in block['content']:
							text += c
						titles += self.parser.replace_marker( title_html, TMPL_MARKER_CONTENT, text )
					elif block['type'] == 'desc':
						text = ''
						for c in block['content']:
							text += self.parser.replace_marker( line_html, TMPL_MARKER_CONTENT, c )
						descs += self.parser.replace_marker( desc_html, TMPL_MARKER_CONTENT, text )
			
			self.clean_media( os.path.join( OUTPUT_IMAGE_FOLDER, p['id'] ) )
			self.clean_media( os.path.join( OUTPUT_VIDEO_FOLDER, p['id'] ) )
			
			# copy thumb
			thumb = { 
				'dst_dir': os.path.join( OUTPUT_IMAGE_FOLDER, p['id'] ),
				'dst': os.path.join( OUTPUT_IMAGE_FOLDER, p['id'], 'thumb.png' ),
				'src': os.path.join( p['path'], 'thumb.png' ) }
			self.publish_media( thumb )
			
			slideshow = ''
			for media in p['data']['slideshow']:
				
				dst_path = ''
				if media['type'] == 'img':
					text = '' + self.template.project_tmpl.slideshow_image
					dst_path = OUTPUT_IMAGE_FOLDER
				elif media['type'] == 'video':
					text = '' + self.template.project_tmpl.slideshow_video
					dst_path = OUTPUT_VIDEO_FOLDER
				else:
					continue
				
				# build html
				text = self.parser.replace_marker( text, TMPL_MARKER_SRC, os.path.join( IMAGE_FOLDER, p['id'], media['src'] ) )
				title = media['src']
				if 'title' in media:
					title = media['title']
				text = self.parser.replace_marker( text, TMPL_MARKER_TITLE, title )
				alt = media['src']
				if 'alt' in media:
					alt = media['alt']
				text = self.parser.replace_marker( text, TMPL_MARKER_ALT, alt )
				if slideshow == '':
					text = self.parser.replace_marker( text, TMPL_MARKER_CLASS, 'first' )
				else:
					text = self.parser.replace_marker( text, TMPL_MARKER_CLASS, '' )
				slideshow += text
				
				# copy media in folder
				media['dst_dir'] = os.path.join( dst_path, p['id'] )
				media['dst'] = os.path.join( dst_path, p['id'], media['src'] )
				media['src'] = os.path.join( p['path'], media['src'] )
				self.publish_media( media )
			
			project_html = self.parser.replace_marker( project_html, TMPL_MARKER_TITLES, titles )
			project_html = self.parser.replace_marker( project_html, TMPL_MARKER_DESCS, descs )
			project_html = self.parser.replace_marker( project_html, TMPL_MARKER_SLIDESHOW, slideshow )
			
			phtml += project_html
			
			pcount += 1
		
		shtml = ''
		smenu = ''
		for s in self.content.sections:
			
			section_html = '' + self.template.section_tmpl.content
			section_html = self.parser.replace_marker( section_html, TMPL_MARKER_ID, s['id'] )
			
			menu_html = None
			if self.template.section_menu_tmpl != None:
				menu_html = '' + self.template.section_menu_tmpl.content
				menu_html = self.parser.replace_marker( menu_html, TMPL_MARKER_ID, s['id'] )
			
			titles = ''
			blocks = ''
			links = ''
			for lang in s['data']['lang']:
				
				title_html = '' + self.template.section_tmpl.section_title
				block_html = '' + self.template.section_tmpl.section_block
				line_html = '' + self.template.section_tmpl.section_blockline
				title_html = self.parser.replace_marker( title_html, TMPL_MARKER_LANG, lang['name'] )
				block_html = self.parser.replace_marker( block_html, TMPL_MARKER_ID, s['id'] )
				block_html = self.parser.replace_marker( block_html, TMPL_MARKER_LANG, lang['name'] )
				
				link_html = None
				if menu_html != None:
					link_html = '' + self.template.section_menu_tmpl.section_link
					link_html = self.parser.replace_marker( link_html, TMPL_MARKER_ID, s['id'] )
					link_html = self.parser.replace_marker( link_html, TMPL_MARKER_LANG, lang['name'] )
				
				for block in lang['content']:
					if block['type'] == 'title':
						text = ''
						for c in block['content']:
							text += c
						titles += self.parser.replace_marker( title_html, TMPL_MARKER_CONTENT, text )
						if link_html != None:
							links += self.parser.replace_marker( link_html, TMPL_MARKER_CONTENT, text )
					elif block['type'] == 'block':
						text = ''
						for c in block['content']:
							text += self.parser.replace_marker( line_html, TMPL_MARKER_CONTENT, c )
						blocks += self.parser.replace_marker( block_html, TMPL_MARKER_CONTENT, text )
				
			section_html = self.parser.replace_marker( section_html, TMPL_MARKER_TITLES, titles )
			section_html = self.parser.replace_marker( section_html, TMPL_MARKER_BLOCKS, blocks )
			shtml += section_html
			
			if menu_html != None:
				m0 = self.parser.replace_marker( menu_html, TMPL_MARKER_CONTENT, links )
				m0 = self.parser.replace_marker( m0, TMPL_MARKER_CLASS, '' )
				m1 = self.parser.replace_marker( menu_html, TMPL_MARKER_CONTENT, links )
				m1 = self.parser.replace_marker( m1, TMPL_MARKER_CLASS, '_screen' )
				smenu += m0 + m1
			
		output = '' + self.template.content
		output = self.parser.replace_marker( output, TMPL_MARKER_PROJECT_LIST,phtml )
		output = self.parser.replace_marker( output, TMPL_MARKER_SECTION_LIST,shtml )
		output = self.parser.replace_marker( output, TMPL_MARKER_SECTION_MENU_LIST, smenu )
		
		soup = bs4.BeautifulSoup(output)
		output = soup.prettify(indent_width=4)
		
		f = open( OUTPUT_FILE, 'w' )
		f.write( output )
		f.close()

# prettify
orig_prettify = bs4.BeautifulSoup.prettify
r = re.compile(r'^(\s*)', re.MULTILINE)
def prettify(self, encoding=None, formatter="minimal", indent_width=4):
    return r.sub(r'\1' * indent_width, orig_prettify(self, encoding, formatter))
bs4.BeautifulSoup.prettify = prettify

# loading template
template = tmpl( TMPL_PATH )
#template.dump()

# loading content
content = content_loader()
#content.dump()

# building output
pbuild = page_builder()
pbuild.set_template( template )
pbuild.set_content( content )
pbuild.build()
