# titre

FR: Tuning Game
NL: Tuning Game
EN: Tuning Game

# description

FR: Ce jeu fait partie d'un projet d'édition de Contredanse sur la chorégraphe américaine Lisa Nelson et sa pratique de la danse. Il s'agit d'une version virtuelle de *tuning scores*, une technique d'improvisation que Lisa a développée au fil des ans.
NL: Dit spel maakt deel uit van een publicatieproject van Contredanse over de Amerikaanse choreografe Lisa Nelson en haar danspraktijk. Het is een virtuele versie van *tuning scores*, een improvisatietechniek die Lisa in de loop der jaren heeft opgebouwd.
EN: This game is part of a publishing project of Contredanse about the US choreographer Lisa Nelson and her dance practice. It is a virtual version of *tuning scores*, an improvisation technique Lisa has built over the years.

# date

FR: En cours de développement, date de sortie prévue: 2021
NL: In ontwikkeling, verwachte verschijningsdatum: 2021
EN: Under development, expected release date: 2021

# collaboration

FR: 
- coordination: [Contredanse ASBL](https://contredanse.org/)
- chorégraphie: [Lisa Nelson](https://en.wikipedia.org/wiki/Lisa_Nelson)
NL:
- coördinatie: [Contredanse ASBL](https://contredanse.org/)
- choreografie: [Lisa Nelson](https://en.wikipedia.org/wiki/Lisa_Nelson)
EN:
- coordination: [Contredanse ASBL](https://contredanse.org/)
- choreography: [Lisa Nelson](https://en.wikipedia.org/wiki/Lisa_Nelson)

# technique

FR: [godot engine](http://www.godotengine.org) avec moteur physique et multi-joueur en ligne
NL: [godot engine](http://www.godotengine.org) met physics engine en online multiplayer
FR: [godot engine](http://www.godotengine.org) with physics engine and online multiplayer

# links:

- [video](https://vimeo.com/222676715)
- [source](https://gitlab.com/polymorphcool/tuning_game)
