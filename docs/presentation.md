# polymorph


-------------------
## présentation (FR)

Polymorph est un studio de recherche et création graphique basé a Bruxelles.

Notre expérience en matière de développement logiciel nous permet de nous adapter facilement à des demandes variées, que ce soit pour le web, le print, les installations interactives, le design d'exposition, les applications ou le jeu vidéo.

Notre but est de trouver une harmonie entre la technique et l'artistique et de la mettre au service des projets.

-------------------
## presentatie (NL)

Polymorph is een onderzoeks- en grafisch ontwerpbureau gevestigd in Brussel.

Onze ervaring in softwareontwikkeling stelt ons in staat om ons gemakkelijk aan te passen aan een verscheidenheid van verzoeken, of het nu gaat om het web, print, interactieve installaties, tentoonstellingsontwerp, toepassingen of videogames.

Ons doel is om een harmonie te vinden tussen het technische en het artistieke en dit ten dienste te stellen van de projecten.

-------------------
## presentation (EN)

Polymorph is a research and graphic design studio based in Brussels.

Our experience in software development allows us to easily adapt to a variety of demands, be it for the web, print, interactive installations, exhibition design, applications or video games.

Our aim is to find a harmony between the technical and the artistic and to put it at the service of the projects.

-------------------
## philosophie (FR)

Nous travaillons avec des programmes et des OS FLOSS. La communauté du libre met l'emphase sur le partage de connaissance ainsi que sur la liberté des utilisateurs à exécuter, copier, distribuer, étudier et améliorer les programmes qu'ils utilisent.

Cette manière de travailler nous pousse à développer une relation de confiance dans toutes nos collaborations, à nous préoccuper des enjeux spécifiques de chaque projets pour aboutir à des réalisations qui sont au plus proches des besoins. Chaque projet a son ADN, notre volonté est de le découvrir avec vous.

Grâces aux licenses et à la documentation que nous lions à nos réalisations, nous assurons aux personnes avec qui nous travaillons une réelle autonomie sur le long terme. Nous rendons transparents nos outils et processus de travail.

Pas de boîte noire avec nous.

-------------------
## filosofie (NL)

We werken met FLOSS-programma's en OSes. De vrije software gemeenschap benadrukt het delen van kennis en de vrijheid van gebruikers om de programma's die ze gebruiken te draaien, te kopiëren, te verspreiden, te bestuderen en te verbeteren.

Deze manier van werken dwingt ons om een vertrouwensrelatie te ontwikkelen in al onze samenwerkingsverbanden, om ons te bekommeren om de specifieke kwesties van elk project om zo dicht mogelijk bij de behoeften te komen. Elk project heeft zijn eigen DNA, en we willen het ontdekken met
jij.

Dankzij de licenties en documentatie die we koppelen aan onze prestaties, verzekeren we de mensen die we werken met een echte autonomie op lange termijn. We maken onze tools en werkprocessen transparant.

Geen zwarte doos bij ons.

-------------------
## philosophy (EN)

We work with FLOSS programs and OSes. The free software community emphasizes the sharing of knowledge and the freedom of users to run, copy, distribute, study and improve the programs they use.

This way of working pushes us to develop a relationship of trust in all our collaborations, to be concerned with the specific stakes of each project in order to achieve ojects that are as close as possible to the needs. Each project has its own DNA, and we want to discover it with you.

Thanks to the licenses and documentation we link to our achievements, we ensure that the people we work with are truly autonomous in the long term. We make our tools and work processes transparent.

No black box with us.

-------------------
## ce que nous savons faire (FR)

### design

- logo
- print
- web
- typographie
- visuels 2d / 3d

### développement

- génération procédurale
- jeux vidéos
- applications web & mobiles
- bots & serveurs applicatifs

- Javascript
- C / C++ / C#
- Python
- OpenGL / WebGL
- PHP
- Java

-------------------
## wat we kunnen doen (NL)

### design

- logo
- print
- web
- typografie
- 2d / 3d beelden

### ontwikkeling

- procedurele generatie
- videospelletjes
- web & mobiele toepassingen
- bots & applicatieservers

- Javascript
- C / C++ / C#
- Python
- OpenGL / WebGL
- PHP
- Java

-------------------
## what we can do (EN)

### design

- logo
- print
- web
- typography
- 2d / 3d visuals

### development

- procedural generation
- video games
- web & mobile applications
- bots & application servers

- Javascript
- C / C++ / C#
- Python
- OpenGL / WebGL
- PHP
- Java

-------------------
## ce que nous voulons faire (FR)

Réaliser des projets qui mélangent plusieurs de nos compétences pour aboutir à des objets qui soient en accord avec les intentions qui les ont fait naître.

Collaborer avec des personnes intéressées par l'exploration narrative, artistique, technique et communicationnelles de nos outils.

Promouvoir le libre comme une pratique saine de création, favorisant la rencontre et le respect des libertés individuelles. 

-------------------
## wat we willen doen (NL)

Het uitvoeren van projecten die verschillende van onze vaardigheden combineren om objecten te bereiken die in lijn zijn met de bedoelingen die eraan ten grondslag liggen.

Werk samen met mensen die geïnteresseerd zijn in de verhalende, artistieke, technische en communicatieve verkenning van onze tools.

Het bevorderen van vrije kunst als een gezonde creatieve praktijk, het aanmoedigen van ontmoetingen en respect voor de individuele vrijheid. 

-------------------
## what we want to do (FR)

Carry out projects that combine several of our skills to produce objects that are in line with the intentions that gave rise to them.

Collaborate with people interested in the narrative, artistic, technical and communicational exploration of our tools.

To promote free expression as a healthy creative practice, favouring encounters and respect for individual freedom.

-------------------
## équipe (FR)

### noyau

#### Amélie Dumont

Typographe de formation, Amélie est une designer graphique et codeuse travaillant aussi bien dans le développement de site web que dans des fablab où elle gère entre autre des ateliers [IOT](https://en.wikipedia.org/wiki/Internet_of_things).

#### François Zajéga

Artiste-programmeur tout-terrain (développement web et 3d) et enseignant en école supérieures depuis 2002, François a acquis une expérience lui permettant d'adresser les enjeux techniques liés à l'utilisation du code dans des projets pluridisciplinaires atistiques ou universitaires.

### réseau

Notre expérience nous a permis de tisser un large réseaux de professionnels dans différents champs: écriture, communication, graphisme imprimé et web, 3d, animation, shooting et post-production vidéo, création musicale, expositions et installation interactives.

En fonction des projets, nous sommes capable de constituer des équipes multi-disciplinaires.

-------------------
## ploeg (NL)

### kernel

#### Amélie Dumont

Amélie is opgeleid tot typograaf en is grafisch ontwerper en codeur. Ze werkt zowel in de ontwikkeling van websites als in fablab waar ze onder andere IOT-workshops beheert.

#### François Zajéga

Ruige kunstenaar-programmeur (web- en 3D-ontwikkeling) en docent in het hoger onderwijs sinds 2002, François heeft een ervaring opgedaan die hem in staat stelt om de technische kwesties met betrekking tot het gebruik van code in multidisciplinaire, atistische of academische projecten aan te pakken.

### netwerk

Onze ervaring heeft ons in staat gesteld een groot netwerk van professionals op te bouwen op verschillende gebieden: schrijven, communicatie, printen en web graphics, 3d, animatie, video-opnames en post-productie, muziekcreatie, interactieve tentoonstellingen en installaties.

Afhankelijk van het project zijn we in staat om multidisciplinaire teams op te zetten.

-------------------
## crew (EN)

### kernel

#### Amélie Dumont

Trained as a typographer, Amélie is a graphic designer and coder working both in website development and in fablab where she manages IOT workshops among other things.

#### François Zajéga

As an all-terrain artist-programmer (web and 3d development) and teacher in higher education since 2002, François has acquired experience that enables him to address the technical issues related to the use of code in multidisciplinary, atistic or academic projects.

### network

Our experience has enabled us to build a wide network of professionals in different fields: writing, comunication, print and web graphics, 3d, animation, video shooting and post-production, music creation, interactive exhibitions and installations.

Depending on the project, we are able to set up multi-disciplinary teams.

-----------------------
trash

Dans nos réalisations, nous mélangeons les media pour donner forme à 
des objets uniques, hybridant l'image, le son et le texte.

En suivant ces principes, nous assurons aux personnes avec qui nous travaillons
une réelle autonomie sur le long terme. Nous rendons transparents nos outils 
et processus de travail.

Grâces aux licenses ainsi qu'au travers de la documentation (visuelle et 
textuelle) que nous lions à nos réalisations, pas de boîte noire avec nous.
