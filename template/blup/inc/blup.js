// french=0, nederlands=1 english=2
var langages = [ 'fr', 'nl', 'en' ];
var langage_classes = [];
var selected_lang = 0;

function langage_select( lang ) {
	if ( langage_classes.length != langages.length ) {
		langage_classes = [];
		for ( var i = 0; i < langages.length; ++i ) {
			langage_classes.push('lang_'+langages[i]);
		}
	}
	if (lang == selected_lang) {
		return;
	}
	selected_lang = lang;
	document.body.className = langage_classes[lang];
	var done = false;
	var url = ''+document.location
	for ( var i = 0; i < langages.length; ++i ) {
		if ( url.indexOf('#l='+langages[i]) != -1 ) {
			url = url.replace('#l='+langages[i], '#l='+langages[selected_lang]);
			done=true;
		}
	}
	if (!done) {
		url += '#l='+langages[selected_lang];
	}
	document.location = url;
	return false;
}

function tabber_open( el, open ) {
	if (open) {
		while ( el.className.indexOf('close') != -1 ) {
			el.className = el.className.replace('close', '');
		}
		if ( el.className.indexOf('open') == -1 ) {
			el.className += ' open';
		}
	} else {
		// do NOT trigger the animation if the element was not open before!
		var found = false;
		while ( el.className.indexOf('open') != -1 ) {
			el.className = el.className.replace('open', '');
			found = true;
		}
		if (found) {
			el.className += ' close';
		}
	}
}

function tabber_click(tabber_id,sub_id) {
	var button_id = tabber_id + '_b' + sub_id;
	var tab_id = tabber_id + '_t' + sub_id;
	var tab_selected = -1;
	var tab_num = 0;
	var tabs = document.getElementById(tabber_id);
	var subs = tabs.getElementsByTagName('div');
	for ( var i = 0; i < subs.length; ++i ) {
		var el = subs[i];
		// button management
		if (el.className.indexOf('taber_button') != -1) {
			if (el.id == button_id) {
				tabber_open( el, true );
			} else {
				tabber_open( el, false );
			}
		} 
		// tabs managenent
		else if (el.className.indexOf('taber_tab') != -1) {
			if (el.id == tab_id) {
				tabber_open( el, true );
				tab_selected = tab_num;
			} else {
				tabber_open( el, false );
			}
			tab_num += 1;
		}
	}
	if ( tab_selected != -1 && tab_selected+1 == tab_num ) {
		tabber_open( tabs, false );
	} else {
		tabber_open( tabs, true );
	}
	return false;
}
