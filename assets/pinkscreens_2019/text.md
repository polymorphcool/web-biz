# titre

FR: Bande-annonce du Pink Screens 2019, le festival du film queer de Bruxelles
NL: Aanhangwagen van Pink Screens 2019, het Brusselse queer filmfestival
EN: Trailer of Pink Screens 2019, Brussels Queer Film Festival

# description

FR: animation 3d
NL: 3d-animatie
EN: 3d animation

# date

FR: Novembre 2019
NL: November 2019
EN: November 2019

# collaboration

FR: 
- réalisateur: [Pablo Diartinez](https://vimeo.com/seeyoustudio)
NL:
- directeur: [Pablo Diartinez](https://vimeo.com/seeyoustudio)
EN:
- director: [Pablo Diartinez](https://vimeo.com/seeyoustudio)

# technique

FR: rendu 3d avec [blender](http://www.blender.org)
NL: 3d rendering met [blender](http://www.blender.org)
FR: 3d rendering with [blender](http://www.blender.org)

# links:

- [video](https://vimeo.com/365967177)
- [site](https://pinkscreens.org)
