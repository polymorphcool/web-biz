# titre

FR: La jeune fille de la mer – Futari Shizuka
NL: Het meisje uit de zee – Futari Shizuka
EN: The Maiden from the sea – Futari Shizuka

# description

FR: visuels temps réels pour l'opéra, Corée
NL: realtime beelden voor opera, Korea
EN: real-time visuals for opera, Korea

# date

FR: du 29/03 au 31/09/2019 (première mondiale)
NL: vanaf 29/03 tot 31/09/2019 (wereldwijde première)
EN: from 29/03 to 31/09/2019 (world premiere)

# collaboration

FR: 
- metteur en scène: [Thomas Israel](http://www.thomasisrael.be)
- régie video: [Antoine Goldschmidt](http://www.magicstreet.be/)
NL:
- theaterregisseur: [Thomas Israel](http://www.thomasisrael.be)
- videocontrole: [Antoine Goldschmidt](http://www.magicstreet.be/)
EN:
- director: [Thomas Israel](http://www.thomasisrael.be)
- video control room: [Antoine Goldschmidt](http://www.magicstreet.be/)

# technique

FR:
[godot engine](http://www.godotengine.org) avec modules personnalisés:
- système de particules avec physique[1](https://gitlab.com/polymorphcool/futari-addon), 
- réseau[2](https://gitlab.com/frankiezafe/gdosc),
- partage de textures[3](https://gitlab.com/frankiezafe/gsyphon).

NL:
[godot engine](http://www.godotengine.org) met aangepaste modules:
- deeltjessysteem met fysica[1](https://gitlab.com/polymorphcool/futari-addon), 
- netwerk[2](https://gitlab.com/frankiezafe/gdosc),
- textuuruitwisseling[3](https://gitlab.com/frankiezafe/gsyphon).

EN:
[godot engine](http://www.godotengine.org) with custom modules:
- particule system with physics[1](https://gitlab.com/polymorphcool/futari-addon), 
- network[2](https://gitlab.com/frankiezafe/gdosc),
- and texture sharing[3](https://gitlab.com/frankiezafe/gsyphon).

# links:

- [site](http://www.thomasisrael.be/pf/futarishizuka/)
- [video](https://vimeo.com/358255087)
