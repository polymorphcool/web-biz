tool

extends Node2D

export(Texture) var texture0:Texture = null
export(Texture) var texture1:Texture = null
export(int) var resolution:int = 200
export(int) var kernel:int = 10
export(bool) var compute_distance:bool = false setget do_compute_distance
export(ImageTexture) var distance:ImageTexture = null

func color_distance( c0:Color, c1:Color ) -> float:
	return abs( c0.r-c1.r ) + abs( c0.g-c1.g ) + abs( c0.b-c1.b )
#	return abs( c0.h-c1.h )

func do_compute_distance(b:bool) -> void:
	compute_distance = false
	if b:
		var src_i0:Image = texture0.get_data()
		var src_i1:Image = texture1.get_data()
		if src_i0.get_size() != src_i1.get_size():
			printerr( "Both images must have the same size" )
		
		var i0:Image = null
		var i1:Image = null
		
		var src_size:Vector2 = src_i0.get_size()
		var ratio:float = 1
		
		var image_size:Vector2 = src_i0.get_size()
		if image_size.x > resolution or image_size.y > resolution:
			# resizing!
			ratio = image_size.x / resolution
			if image_size.x < image_size.y:
				ratio = image_size.y / resolution
			image_size = Vector2( int( image_size.x / ratio ), int( image_size.y / ratio ) )
			i0 = Image.new()
			i0.copy_from( src_i0 )
			i0.resize( image_size.x, image_size.y, Image.INTERPOLATE_CUBIC )
			i1 = Image.new()
			i1.copy_from( src_i1 )
			i1.resize( image_size.x, image_size.y, Image.INTERPOLATE_CUBIC )
		else:
			i0 = src_i0
			i1 = src_i1
		
		i0.lock()
		i1.lock()
		var width:int = image_size.x
		var height:int = image_size.y
		var total:int = width * height
		var pixels0:Array = []
		var pixels1:Array = []
		for y in height:
			for x in width:
				pixels0.append( i0.get_pixel(x,y) )
				pixels1.append( i1.get_pixel(x,y) )
		i0.unlock()
		i1.unlock()
		
		# now we can compute the proximity between pixels
		var di:Image = Image.new()
		di.create( width, height, false, Image.FORMAT_RGF )
		di.lock()
		for i in range( 0, total ):
			var needle:Color = pixels0[i]
			var closest:int = -1
			var shortestc:float = 0.8
			var src_x:int = i % width
			var src_y:int = i / width
			for y in range( max( 0, src_y - kernel ), min( height, src_y + kernel ) ):
				for x in range( max( 0, src_x - kernel ), min( width, src_x + kernel ) ):
					var pid:int = x + y * width
					var dx:int = pid % width
					var dy:int = pid / width
					var dp:float = ( pow( src_x - dx, 2 ) + pow( src_y - dy, 2 ) ) / (kernel*kernel)
					if dp >= 1:
						continue
					var dc:float = color_distance( needle, pixels1[pid] ) * ( 1 + pow(dp,3) )
					if shortestc > dc:
						shortestc = dc
						closest = pid
					elif !closest == 1 and shortestc == dc:
						var cx:int = closest % width
						var cy:int = closest / width
						if ( cx*cx+cy*cy > dx*dx+dy*dy ):
							closest = pid
						
			if closest == -1:
				closest = i
			var dst_x:int = closest % width
			var dst_y:int = closest / width
			var offsetx:float = ( dst_x - src_x ) / image_size.x
			var offsety:float = ( dst_y - src_y ) / image_size.y
			di.set_pixel( src_x, src_y, Color( offsetx, offsety, 0.0 ) )
			if i > 0 and i % 200 == 0:
				print( float(int(((i+1)*1.0/total)*10000)) / 100, "% pixels done" )
		di.unlock()
		
		distance = ImageTexture.new()
		distance.create_from_image( di, Texture.FLAG_FILTER )
