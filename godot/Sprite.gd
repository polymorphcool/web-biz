extends Sprite

export (float,0,1000) var speed:float = 5

onready var dir:Vector2 = Vector2(rand_range(-1,1),rand_range(-1,1)).normalized()

func _ready():
	pass

func _process(delta):
	var vps:Vector2 = get_viewport().size
	if position.x + dir.x * speed < 0 and dir.x < 0:
		dir.x *= -1
	elif position.x + dir.x * speed > vps.x and dir.x > 0:
		dir.x *= -1
	if position.y + dir.y * speed < 0 and dir.y < 0:
		dir.y *= -1
	elif position.y + dir.y * speed > vps.y and dir.y > 0:
		dir.y *= -1

	position += dir * speed * delta
